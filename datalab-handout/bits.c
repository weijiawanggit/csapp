/* 
 * CS:APP Data Lab 
 * 
 * <Please put your name and userid here>
 * 
 * bits.c - Source file with your solutions to the Lab.
 *          This is the file you will hand in to your instructor.
 *
 * WARNING: Do not include the <stdio.h> header; it confuses the dlc
 * compiler. You can still use printf for debugging without including
 * <stdio.h>, although you might get a compiler warning. In general,
 * it's not good practice to ignore compiler warnings, but in this
 * case it's OK.  
 */


// #include  <stdio.h>
#if 0
/*
 * Instructions to Students:
 *
 * STEP 1: Read the following instructions carefully.
 */

You will provide your solution to the Data Lab by
editing the collection of functions in this source file.

INTEGER CODING RULES:
 
  Replace the "return" statement in each function with one
  or more lines of C code that implements the function. Your code 
  must conform to the following style:
 
  int Funct(arg1, arg2, ...) {
      /* brief description of how your implementation works */
      int var1 = Expr1;
      ...
      int varM = ExprM;

      varJ = ExprJ;
      ...
      varN = ExprN;
      return ExprR;
  }

  Each "Expr" is an expression using ONLY the following:
  1. Integer constants 0 through 255 (0xFF), inclusive. You are
      not allowed to use big constants such as 0xffffffff.
  2. Function arguments and local variables (no global variables).
  3. Unary integer operations ! ~
  4. Binary integer operations & ^ | + << >>
    
  Some of the problems restrict the set of allowed operators even further.
  Each "Expr" may consist of multiple operators. You are not restricted to
  one operator per line.

  You are expressly forbidden to:
  1. Use any control constructs such as if, do, while, for, switch, etc.
  2. Define or use any macros.
  3. Define any additional functions in this file.
  4. Call any functions.
  5. Use any other operations, such as &&, ||, -, or ?:
  6. Use any form of casting.
  7. Use any data type other than int.  This implies that you
     cannot use arrays, structs, or unions.

 
  You may assume that your machine:
  1. Uses 2s complement, 32-bit representations of integers.
  2. Performs right shifts arithmetically.
  3. Has unpredictable behavior when shifting an integer by more
     than the word size.

EXAMPLES OF ACCEPTABLE CODING STYLE:
  /*
   * pow2plus1 - returns 2^x + 1, where 0 <= x <= 31
   */
  int pow2plus1(int x) {
     /* exploit ability of shifts to compute powers of 2 */
     return (1 << x) + 1;
  }

  /*
   * pow2plus4 - returns 2^x + 4, where 0 <= x <= 31
   */
  int pow2plus4(int x) {
     /* exploit ability of shifts to compute powers of 2 */
     int result = (1 << x);
     result += 4;
     return result;
  }

FLOATING POINT CODING RULES

For the problems that require you to implent floating-point operations,
the coding rules are less strict.  You are allowed to use looping and
conditional control.  You are allowed to use both ints and unsigneds.
You can use arbitrary integer and unsigned constants.

You are expressly forbidden to:
  1. Define or use any macros.
  2. Define any additional functions in this file.
  3. Call any functions.
  4. Use any form of casting.
  5. Use any data type other than int or unsigned.  This means that you
     cannot use arrays, structs, or unions.
  6. Use any floating point data types, operations, or constants.


NOTES:
  1. Use the dlc (data lab checker) compiler (described in the handout) to 
     check the legality of your solutions.
  2. Each function has a maximum number of operators (! ~ & ^ | + << >>)
     that you are allowed to use for your implementation of the function. 
     The max operator count is checked by dlc. Note that '=' is not 
     counted; you may use as many of these as you want without penalty.
  3. Use the btest test harness to check your functions for correctness.
  4. Use the BDD checker to formally verify your functions
  5. The maximum number of ops for each function is given in the
     header comment for each function. If there are any inconsistencies 
     between the maximum ops in the writeup and in this file, consider
     this file the authoritative source.

/*
 * STEP 2: Modify the following functions according the coding rules.
 * 
 *   IMPORTANT. TO AVOID GRADING SURPRISES:
 *   1. Use the dlc compiler to check that your solutions conform
 *      to the coding rules.
 *   2. Use the BDD checker to formally verify that your solutions produce 
 *      the correct answers.
 */


#endif
/* 
 * bitAnd - x&y using only ~ and | 
 *   Example: bitAnd(6, 5) = 4
 *   Legal ops: ~ |
 *   Max ops: 8
 *   Rating: 1
 */
int bitAnd(int x, int y) {
  return ~((~x)|(~y));
}


// use morgen theory
// #################### //

// the truth table
//    x    0    1
//  y      
//  0      0    0
//  1      0    1

// the truth table of ~x and ~y
//    ~x   1    0
//  ~y      
//  1      0    0
//  0      0    1


/* 
 * getByte - Extract byte n from word x
 *   Bytes numbered from 0 (LSB) to 3 (MSB)
 *   Examples: getByte(0x12345678,1) = 0x56
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 6
 *   Rating: 2
 */
int getByte(int x, int n){

  int mask;
  int result;

  // printf("the n is: %d,  %x \n", n, n);
  // printf("the (n<<3)  is: %d,  %x \n", (n<<3) , (n<<3) );

  mask = 0xff << (n<<3);
  // printf("the hex value of the mask is: %d,  %x \n", mask, mask);

  result =  x & mask ;   // and in bits, choose the number "n" bytes;
  // printf("the result after mask is : %d,  %x \n", result, result);


  result = result >> (n<<3);  // then move the select bytes back to the LSB position;
  // because should use result as unsigned, the shift is logical shift, not arithmetic one.
  // but we can not use unsigned, so here needs to mask a 0x000000ff, only takes the last byte as result

  result = result & 0x000000ff;


  // printf("the hex value of the result is: %d,  %x \n", result, result);

  return result;

}
/* 
 *   logicalShift - shift x to the right by n, using a logical shift
 *   Can assume that 0 <= n <= 31
 *   Examples: logicalShift(0x87654321,4) = 0x08765432
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 20
 *   Rating: 3 
 */
// right logical shift
int logicalShift(int x, int n) 
{

  int mask;
  int mask_all;  // the mask for 0xffffffff or 0x00000000 by judge if the n is 0 or not
  int mask_1bit;
  int result;

  int bool_n;

  
  result = x >> n;
  // 1. first, shift the result to the right n bits, no matter what is the first n bits;
  // printf("the result after mask is : %d,  %x \n", result, result);

  bool_n = !!n;  // if n != 0, n == 1, if n == 0, then n == 0
  // printf("the result of bool_n is : %d,  %x \n", bool_n, bool_n);
  
  mask_1bit = bool_n << 31;  // the first bit is 0 or 1;
  // this will generate a 0x8000_0000 or 0x00000000

  // printf("the mask_1bit now is : %d,  %x \n", mask_1bit, mask_1bit);


  mask_all = mask_1bit >> 31;
  // printf("the mask_all now is : %d,  %x \n", mask_all, mask_all);
  // here need to use the bool_n to make a 0xffffffff or 0x00000000
  // then use this to make (n-1), or 0

  // mask = mask_1bit >> ( (n+(~1)) && bool_n);
  // mask = mask_1bit >> ( (n+(~~bool_n)));

  // printf(" (n): %d,  %x \n", (n), (n)  );
  // printf(" (~1): %d,  %x \n", (~1), (~1) );  // this is -2
  // printf(" (~1)+1: %d,  %x \n", (~1)+1, (~1)+1 );   // this is -1
  // printf(" (n+(~1)+1): %d,  %x \n", (n+(~1)+1), (n+(~1)+1)  );
  // printf(" (n+(~1)+1) & mask_all: %d,  %x\n", (n+(~1)+1) & mask_all, (n+(~1)+1) & mask_all);
  


  mask = mask_1bit >> ((n+(~1)+1) & mask_all);
  // (n+(~1)+1) mask with 0xffff_ffff or 0x0000_0000
  // printf("the mask now is : %d,  %x \n", mask, mask);

  mask = ~mask;   // reverse the bit at bits level
  // printf("the mask after reverse now is : %d,  %x \n", mask, mask);

  result = result & mask;
  // printf("the result now is : %d,  %x \n", result, result);

  return result;
}
/*
 * bitCount - returns count of number of 1's in word
 *   Examples: bitCount(5) = 2, bitCount(7) = 3
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 40
 *   Rating: 4
 */
int bitCount(int x) {

  // form a 32 bits list and shift to count
  // use mask1 to count bits in every 4 bits with right shift
  // add count together in every 4 bits by divide them into 8 and then 4
  // since we use every 4 bits to count, last 4 is just the count
  
  // Besides, we do not need to worry about overflow for every 4 bits
  // since the maximum count for 4 bits is 0100b, we can reserve them

  int sum = 0;
  int byte0;
  int byte1;
  int byte2;
  int byte3;
  int byte4;
  int byte5;
  int byte6;
  int byte7;


  int mask = 0xff | 0xff<<8;   // 16 bits width
  int mask0 = 0x11 | 0x11<<8 ;  // 16 bits width
  // int mask = 0xffff;
  // int mask0 = 0x1111;
  
  int mask1 = mask0 | (mask0 << 16);


  // printf(" mask: %d,  %x \n", mask, mask );
  // printf(" mask1: %d,  %x \n", mask0, mask0 );

  int check;
  int check0 = x & mask1;
  int check1 = (x >> 1 & mask1);
  int check2 = (x >> 2 & mask1);
  int check3 = (x >> 3 & mask1);

  check = check0 + check1 + check2 + check3;




  byte0 = check & 0xf;
  byte1 = (check>>4) & 0xf; 
  byte2 = (check>>8) & 0xf; 
  byte3 = (check>>12) & 0xf; 

  byte4 = (check>>16) & 0xf; 
  byte5 = (check>>20) & 0xf; 
  byte6 = (check>>24) & 0xf; 
  byte7 = (check>>28) & 0xf; 

  sum = byte0 + byte1 + byte2 + byte3 + byte4 + byte5 + byte6 + byte7;
  // printf(" sum is: %d,  %x \n", sum, sum );

  return sum;
}



/* 
 * bang - Compute !x without using !
 *   Examples: bang(3) = 0, bang(0) = 1
 *   Legal ops: ~ & ^ | + << >>
 *   Max ops: 12
 *   Rating: 4 
 */
int bang(int x) {

  // int temp_ff;  // test if the x minus one equals to 0xffff_ffff or not  
  // int temp_zeros;
  // temp_ff = x+(~1)+1;   // means x - 1

  // temp_zeros = ~temp_ff;  // if the input is 0, here should be all zeros
  int front_16;
  int front_8;
  int front_4;
  int front_2;
  int front_1;
  int first_bit;

  front_16 = x | (x<<16);                 // only care the front 16 bits of front_16
  front_8 = front_16 | (front_16<<8);     // only care the front 8 bits of front_8
  front_4 = front_8 | (front_8<<4);       // only care the front 4 bits of front_4
  front_2 = front_4 | (front_4<<2);       // only care the front 2 bits of front_2
  front_1 = front_2 | (front_2<<1);       // only care the front 1 bit of front_1

  first_bit = (front_1>>31)+1;   // do a arithmetic right move, keep the first bits for all bits, so the result will be 0x0000_0000 or 0xffff_ffff
  return first_bit;
}



/* 
 * tmin - return minimum two's complement integer 
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 4
 *   Rating: 1
 */
int tmin(void) {
  // gives the Tmin of int
  // the first bit is one, others are zeros
  int one = 0x01;
  int Tmin;  

  Tmin = one<<31;

  return Tmin;
}




/* 
 * fitsBits:
 *   return 1 if x can be represented as an 
 *   n-bit, two's complement integer.
 *   1 <= n <= 32
 *   Examples: fitsBits(5,3) = 0, fitsBits(-4,3) = 1
 *   ....0101  if only use 3 bits, it is not 5, but a minus number
 *   ....100,  -2^(2)+0+0 = -4, it could use only 3 bits to represent
 * 
 * 
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 15
 *   Rating: 2
 */
int fitsBits(int x, int n) {


  // we can use right shift to leave some bits
  // if they are all one or all zero, then we can consider this as valid
  int useful = n + ~1 + 1;  // (~1 +1) is -1 in int
  // printf(" ~1 + 1 is: %d,  %x \n", ~1 + 1, ~1 + 1 );
  // printf(" useful is: %d,  %x \n", useful, useful );


  int shifted = x >> useful;
  int one = ~0;
  int result = ((!(shifted ^ 0)) | (!(shifted ^ one)));
  return result;



  return 2;
}



/* 
 * divpwr2 - Compute x/(2^n), for 0 <= n <= 30
 *  Round toward zero
 *   Examples: divpwr2(15,1) = 7, divpwr2(-33,4) = -2
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 15
 *   Rating: 2
 */
int divpwr2(int x, int n) {
    return 2;
}
/* 
 * negate - return -x 
 *   Example: negate(1) = -1.
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 5
 *   Rating: 2
 */
int negate(int x) {
  return 2;
}
/* 
 *   isPositive - return 1 if x > 0, 
     return 0 otherwise (include 0 and negtive)
 *   Example: isPositive(-1) = 0.
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 8
 *   Rating: 3
 */
int isPositive(int x) {

  int first_bit;
  int positive;
  int result;
  
  first_bit = x >> 31;  // this will be all 0 or all one;
  // if it is positive or zero, it should be a 0x0000_0000;

  // if x = 0x0, then (!x) == 0x01
  // if x > 0, then (!x) == 0x00

  // then use this 0x0000_0000 | this (!x)
  // if this is a positive number, then the result will be 0x00
  // if this is a zero, then the result will be 0x01
  // if this is a negtive number, then the result will be 0xffff_ffff | 0x01, this is 0x01


  result = first_bit | (!x);



  // if this is a negtive number, then it will be 0xffff_ffff

  // printf("first_bit %d,  %x \n", first_bit, first_bit);
  // printf("result %d,  %x \n", result, result);
  
  // positive = !!(first_bit);

  return !result;
}
/* 
 * isLessOrEqual - if x <= y  then return 1, else return 0 
 *   Example: isLessOrEqual(4,5) = 1.
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 24
 *   Rating: 3
 */
int isLessOrEqual(int x, int y) {
  return 2;
}
/*
 * ilog2 - return floor(log base 2 of x), where x > 0
 *   Example: ilog2(16) = 4
 *   Legal ops: ! ~ & ^ | + << >>
 *   Max ops: 90
 *   Rating: 4
 */
int ilog2(int x) {
  return 2;
}




/* 
 *   float_neg
 *   -- Return bit-level equivalent of expression -f for
 *   floating point argument f.
 *   Both the argument and result are passed as unsigned int's, but
 *   they are to be interpreted as the bit-level representations of
 *   single-precision floating point values.
 * 
 *   When argument is NaN, return argument. (return the unsigned uf)
 *   Legal ops: Any integer/unsigned operations incl. ||, &&. also if, while
 *   Max ops: 10
 *   Rating: 2
 */

unsigned float_neg(unsigned uf) {

  int exp;         // used to store the exp part of floating point repre;
  int uf_shifted;  // used to store the right shift result of uf;
  int s;           // s = 0, maybe not used;
  int frac;        // the fraction part
  int frac_mask;
  unsigned result;

  // uf_shifted = uf;
  // when shift the uf, it is logical shift, NOT arithmetic shift.
  // this means it always put 0 at the left.
  exp = (uf >> 23) & 0xff;

  frac_mask = ~(0x1ff << 23);  // 1 1111 1111 0000 0000 ... -> // 0 0000 0000 1111 1111 ..
  frac = (uf & (frac_mask));
  // printf("exp of uf extracted is %d,  %x \n", exp, exp);
  // printf("frac %d,  %x \n", frac, frac);

  // if (exp != 0xf)   // it is unnormalized or normalized, just reverse the first bit.
  // {
  //   return uf ^ (0x01 << 31);
  //   // here use the XOR operation:
  //   // XOR 1 means reverse the bit
  //   // XOR 0 means keep the bit
  // }
  // else if ((exp == 0xf) && (frac == 0x0))
  // {
  //   return uf ^ (0x01 << 31);
  // }
  // else
  // { 
  //   return uf;
  // }


  if ((exp == 0xff) && (frac != 0x0))   // it is unnormalized or normalized, just reverse the first bit.
  {
    return uf;
    // here use the XOR operation:
    // XOR 1 means reverse the bit
    // XOR 0 means keep the bit
  }
  else
  { 
    return uf ^ (0x01 << 31);
  }

    // for example, uf=1:     e = the E = 0, e= frac = 0, therefore M = 1.
    // e.g.:        uf=1.5:   the E = 0,
    // e.g.:        uf=2:     the E = 1,
}




/*
unsigned float_neg(unsigned uf) {

  int e = 0;  // used to store the E of floating point repre;
  int uf_shifted;  // used to keep the right shift result of uf;
  int s;   // s = 0, maybe not used;
  int frac;   // the fraction part
  int result;

  uf_shifted = uf;
  // when shift the uf, it is logical shift, NOT arithmetic shift.
  // this means it always put 0 at the left. 
  

  printf("e %d,  %x \n", e, e);
  printf("uf_shifted %d,  %x \n", uf_shifted, uf_shifted);

  if (uf == 0)
  {
    e= 0;
    result = 0
  }
  else
  {
    while(uf & 0xffffffff)
    {
        e++;
        uf_shifted == uf_shifted << e;
        printf("e %d,  %x \n", e, e);
    }
    // here calculate the f, and result
    frac = uf << e;   
    
    
    // for example, uf=1:     e = the E = 0, e= frac = 0, therefore M = 1.
    // e.g.:        uf=1.5:   the E = 0,
    // e.g.:        uf=2:     the E = 1,
}
return result;
}
*/





/* 
 * float_i2f - Return bit-level equivalent of expression (float) x
 *   Result is returned as unsigned int, but
 *   it is to be interpreted as the bit-level representation of a
 *   single-precision floating point values.
 *   Legal ops: Any integer/unsigned operations incl. ||, &&. also if, while
 *   Max ops: 30
 *   Rating: 4
 */
unsigned float_i2f(int x) {
  // this function transfers the int into float 

  // copied from the answers/bits.c
  // First we name 0 as 0 and -2^31 as 0xCF000000
  // Then we consider about negative number to fetch their abs number to use ~x+1
  // And we want to use shift op to find their MSB to count their actual exponent=E


  // Besides, since we have 31 bits to put number, float only have (23+1)bits for int( first default 1)
  // For M, we can just use 0x7FFFFF to preserve(or we call mask) 23 bits for it.
  // Therefore, we need to preserve lost precision to round them to even
  // This task can be achieved by &ff, and use 0x80 and 0x7f to judge if lostpart is larger than 0.5
  // After rounding to even, we may check if M has overflow since we may add 1 to make it to be 0x800000
  // If so, we need to exp++ and make m_temp = 0;
  // At last, we can combine those part together with | op
  
  
  int big_E = 0;      // used to store the E of floating point repre;
  int e = 0;
  int x_shifted;  // used to keep the right shift result of uf;
  int s;      // used to store the sign bit
  int frac;   // the fraction part
  int true_neg_value;
  int s_true_value;
  int mask;
  int counts;
  unsigned result;

  unsigned m_temp = 0;
  unsigned lost_precision = 0;
  unsigned rest_precision = 0;
  unsigned large_precision = 0;
  unsigned half_judge = 0;


  //   1xxxx or 0xxxx & 0x8000_0000
  s = (x & (0x01 << 31));  // it should be 0x8000_0000 or 0x0000_0000
  // printf("sign bit is %d,  %x \n", s, s);


  
  // when shift the x, it is logical shift, NOT arithmetic shift.
  // this means it always put 0 at the left.
  

  // printf("big_E %d,  %x \n", big_E, big_E);
  // printf("x_shifted %d,  %x \n", x_shifted, x_shifted);

  if (x == 0)
  {
    result = 0;
  }
  else if( x == 0x80000000){
    result =  0xcf000000;
  }
  else if (s==0)
  {
    x_shifted = x;
    while(x_shifted | 0x0)   // when x_shifted have one still, then continue, when x_shifted is all zero, then go out
    {
      big_E++;
      x_shifted = x >> big_E;
      // printf("big_E %d,  %x \n", big_E, big_E);
    }
    
    // calculate the final big E, 2^E:
    big_E = big_E - 1;
    // printf("big_E %d,  %x \n", big_E, big_E);
    
    // calculate the small e:
    e = big_E + 127;

    // calculate the e in the final result
    e = e << 23;
    // printf("e %d,  %x \n", e, e);

    // here calculate the f, and result
    
    // move the higest bit to the most left
    m_temp = x << (31-big_E);  // this is the abs
    // printf("m_temp %d,  %x \n", m_temp, m_temp);

    lost_precision = m_temp & 0xff;
    // printf("lost_precision %d,  %x \n", lost_precision, lost_precision);

    m_temp = (m_temp >> 8) & 0x7fffff;


    // when move the m_temp >> 8, it will lost this last right 8 bits
    // lost_precision = (x << (31-big_E)) & 0xff;

    // larger precision is the highest part of the lost_precision
    large_precision = lost_precision & 0x80;
    // printf("large_precision %d,  %x \n", large_precision, large_precision);
    // rest_precision is the left part of the lost_precision
    rest_precision = lost_precision & 0x7f;
    // printf("rest_precision %d,  %x \n", rest_precision, rest_precision);
    // to judge if lost_precision is larger than half or half with 1 in head, add 1 for m
    half_judge = large_precision && rest_precision;
    // printf("half_judge %d,  %x \n", half_judge, half_judge);

    m_temp = m_temp + (half_judge || (large_precision && (m_temp & 0x1)));
    // printf("final m_temp %d,  %x \n", m_temp, m_temp);
  
    // if add 1 for m, it may lead m to overflow to 1000..0(24bits)
    // then we need to make m=000..0, and add 1 for exp
    if(m_temp >> 23){
      m_temp = 0;
      e = e + 1;
    }


    frac = m_temp;

    /*
    else if ((x > 0x08000000) & (x < 0x09000000))
    {
      printf("frac in ((x > 0x08000000) & (x < 0x09000000)) \n");
      // frac = (x - (0x1<<big_E)) >> (big_E - 23) ;
      frac = (x - (0x1<<big_E)) ;
      printf("frac %d,  %x \n", frac, frac);
      // frac = frac & 0x007fffff;
    }
    else
    {

    }
    */
    // M = 1 + f

    // (x - (0x1<<big_E))
    // printf("(x - (0x1<<big_E)) %d,  %x \n", (x - (0x1<<big_E)), (x - (0x1<<big_E)));
    // printf("frac %d,  %x \n", frac, frac);
    
    // for example, uf=1:     e = the E = 0, e= frac = 0, therefore M = 1.
    // e.g.:        uf=1.5:   the E = 0,
    // e.g.:        uf=2:     the E = 1,
    result = s + e + frac;
    // printf("result %d,  %x \n", result, result);
  }
  else  // s == 1, the x is negtive number
  {
    
    // printf("s==1 \n");
    x_shifted = s - (x & 0x7fffffff);  // the highest bit minus other lower bits
    true_neg_value = s - (x & 0x7fffffff);
    

    // printf("x_shifted %d,  %x \n", x_shifted, x_shifted);
    while(x_shifted | 0x0)   // when x_shifted have one still, then continue, when x_shifted is all zero, then go out
    {
      big_E++;
      x_shifted = true_neg_value >> big_E;
      // printf("big_E %d,  %x \n", big_E, big_E);
    }
    
    // calculate the final big E, 2^E:
    big_E = big_E - 1;
    // printf("big_E %d,  %x \n", big_E, big_E);
    
    // calculate the small e:
    e = big_E + 127;

    // calculate the e in the final result
    e = e << 23;
    // printf("e %d,  %x \n", e, e);

    // printf("big_E %d,  %x \n", big_E, big_E);
    // printf("true_neg_value %d,  %x \n", true_neg_value, true_neg_value);
    // printf("s_true_value %d,  %x \n", s_true_value, s_true_value);




    
    // here calculate the f, and result
    
    // move the higest bit to the most left

    // true_neg_value is the obs
    m_temp = true_neg_value << (31-big_E);  // this is the abs on the left
    // printf("m_temp %d,  %x \n", m_temp, m_temp);



    lost_precision = m_temp & 0xff;
    // printf("lost_precision %d,  %x \n", lost_precision, lost_precision);

    m_temp = (m_temp >> 8) & 0x7fffff;
    // printf("m_temp after lose the lower 8bits %d,  %x \n", m_temp, m_temp);

    // when move the m_temp >> 8, it will lost this last right 8 bits
    // lost_precision = (x << (31-big_E)) & 0xff;

    // larger precision is the highest part of the lost_precision
    large_precision = lost_precision & 0x80;
    // printf("large_precision %d,  %x \n", large_precision, large_precision);
    // rest_precision is the left part of the lost_precision
    rest_precision = lost_precision & 0x7f;
    // printf("rest_precision %d,  %x \n", rest_precision, rest_precision);
    // to judge if lost_precision is larger than half or half with 1 in head, add 1 for m
    half_judge = large_precision && rest_precision;
    // printf("half_judge %d,  %x \n", half_judge, half_judge);

    m_temp = m_temp + (half_judge || (large_precision && (m_temp & 0x1)));
    // printf("final m_temp %d,  %x \n", m_temp, m_temp);
  
    // if add 1 for m, it may lead m to overflow to 1000..0(24bits)
    // then we need to make m=000..0, and add 1 for exp
    if(m_temp >> 23){
      m_temp = 0;
      e = e + (0x01<<23);
    }


    frac = m_temp;






    // M = 1 + f
    result = s + e + frac;



    // result = 0;
  }




  return result;



}
/* 
 * float_twice - Return bit-level equivalent of expression 2*f for
 *   floating point argument f.
 *   Both the argument and result are passed as unsigned int's, but
 *   they are to be interpreted as the bit-level representation of
 *   single-precision floating point values.
 *   When argument is NaN, return argument
 *   Legal ops: Any integer/unsigned operations incl. ||, &&. also if, while
 *   Max ops: 30
 *   Rating: 4
 */
unsigned float_twice(unsigned uf) {
  return 2;
}
